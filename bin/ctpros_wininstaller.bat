@echo off

set pwd=%cd%
echo Downloading Visual Studio Build tools && echo.
curl -SL --output vs_buildtools.exe https://aka.ms/vs/16/release/vs_buildtools.exe
echo. && echo Installing Build Tools...
start /w vs_buildtools.exe --wait --passive --norestart --add Microsoft.VisualStudio.Workload.VCTools
del /q vs_buildtools.exe

echo Downloading Python 3.7.9: && echo.
curl -SL --output python-3.7.9-amd64.exe https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe

echo. && echo Installing Python...
python-3.7.9-amd64 -passive
del /q python-3.7.9-amd64.exe

echo. && echo Installing Virtual Environment...
cd "%USERPROFILE%\AppData\Local\Programs\Python\Python37"
python -m  venv venvs/ctpros
call venvs/ctpros/Scripts/activate

echo. && echo Installing ctpros...
pip install -U ctpros

echo. && echo Generating %pwd%\ctpros.bat
echo @echo off  > "%pwd%\ctpros.bat"
echo echo ctpros is launching! >> "%pwd%\ctpros.bat"
echo call "%cd%\venvs\ctpros\Scripts\activate" ^&^& start pythonw.exe -m ctpros ^> ^n^u^l  ^&^& deactivate >> "%pwd%\ctpros.bat"
echo @echo on >> "%pwd%\ctpros.bat"

echo. && echo All done! You can run ctpros by going to %pwd%\ and running ctpros.bat.
pause
cd %pwd%
deactivate
@echo on
