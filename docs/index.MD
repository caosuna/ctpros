# Documentation


## Pipelines
### [Registration of Longitudinal Bone](pipeline_registration.md)
### [Stitching Images](pipeline_stitch.md)

## Guides
### [GUI Controls](helpme_gui_controls.md)

## [Return to the README](../README.md)