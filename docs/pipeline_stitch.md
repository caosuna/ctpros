# Stitching Pipeline
Image stitching is the process of generating a combined image from a set of images with overlapping regions. Following image registration, the relationship of image spaces is defined and permits the resampling of the set of images into a single image space.

Common reasons why this is useful:
- permits a piece-wise image acquisition of samples which are otherwise challenging to image in a single session due to geometric constraints
- improve the signal-to-noise ratio of structures which are unable to be imaged for extended periods of time

## Requirements
In order to stitch your images, registration is required to define the spatial relationship between sets of images.

## Example
A set of 4 images is needs to be stitched:
- image1.npy
- image2.npy
- image3.npy
- image4.npy

A minimum of 3 transformations is required to "chain" together the spatial relationship of these images. So long as each image is related to another, the images can be stitched together. The relationships don't necessarily need to be organized in any particular way. The following is appropriate:
- image1-image4.tfm
- image3-image4.tfm
- image4-image2.tfm

Note that the transforms defines an unorganized mapping between all images:
```
           image3
             |
             v
image1 --> image4 --> image2
```

### GUI
1. Open all the images to be stitched
2. Align all the images using `Protocols` --> `Align to ...`
   - `Align to Reference` aligns all the loaded images to a specific image's orientation
     - If a secondary image is selected, it is used as the reference
     - If no secondary image is selected, but a primary image is, it is used in lieu
   - `Align to Average` calculates the average orientation between all the images, then aligns the images to this orientation
3. Stitch all the images using `Protocols` --> `Stitch`
   - Naming of the result is requested to save to the disk.
   - The result is then selected as the primary image with no secondary image to view the result.

### API
See the [JupyterLab Notebook example](../notebooks/example_stitching.ipynb).
