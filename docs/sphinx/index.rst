.. ctpros documentation master file, created by
   sphinx-quickstart on Sat Aug 28 13:06:53 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ctpros's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ctpros


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
