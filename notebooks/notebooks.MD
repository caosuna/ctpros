# JupyterLab Notebooks
Guides for code/API usage examples.

Note:
- Some of these examples utilize Python libraries which are not necessarily dependencies of the module. If following along, be sure to `pip install` the libraries imported alongside `ctpros`
## Notebooks
### [Global Thresholding](example_threshold.ipynb)
### [ctpros and NumPy](example_combine_numpy.ipynb)
### [Batching](example_batching.ipynb)

## Return to [README](../README.md)