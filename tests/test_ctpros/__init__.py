# Forces all loading to occur before testing, keeping profiling clean except when testing is verbose
import sys

if "-vv" not in sys.argv:
    import itkConfig

    itkConfig.LazyLoading = False
    import itk
    from ctpros import modules
    import layz_import

    for module in modules:
        layz_import.layz_module(module).__name__
