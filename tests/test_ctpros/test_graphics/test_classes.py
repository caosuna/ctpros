import unittest, os
import numpy as np

from ctpros import img
from ctpros.graphics.main import *


class TestSet(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.GUI = lambda *imgs: GUI(*imgs, withdraw=True, verbosity=False)
        self.aim3name = os.path.sep.join(["data", "input", "test_v3.aim;1"])
        self.aim2name = os.path.sep.join(["data", "input", "test_v2.aim;1"])
        self.npy3name = os.path.sep.join(["data", "input", "test_v3.npy"])
        self.npy2name = os.path.sep.join(["data", "input", "test_v2.npy"])
        self.aim2gobj = os.path.sep.join(["data", "input", "test_v2_gobj.aim;1"])
        self.affine = os.path.sep.join(["data", "input", "test_affine.tfm"])
        self.jpgname = os.path.sep.join(["data", "input", "test_grayjpg.jpg"])

    def setUp(self):
        self.npy2 = img.NDArray(self.npy2name, verbosity=0)
        self.npy3 = img.NDArray(self.npy3name, verbosity=0)
        self.aim2 = img.AIM(self.aim2name, verbosity=0)
        self.aim3 = img.AIM(self.aim3name, verbosity=0)

    def tearDown(self):
        pass

    def test_init_0imgs(self):
        gui = self.GUI()

        for dropdown in gui.imgselect.dropdowns:
            self.assertEqual(dropdown._variable.get(), "None")
            self.assertEqual(dropdown.get_options(), ["None"])

        for entry in gui.imgpos.entries:
            self.assertEqual(entry.get(), "")

        for scale in gui.imgpos.scales:
            self.assertEqual(scale.cget("from"), scale.cget("to"))
            self.assertEqual(scale.cget("from"), 0)
            self.assertEqual(scale.get(), 0)

        for entries in [
            gui.voiinfo.posentries,
            gui.voiinfo.shapeentries,
            gui.voiinfo.elsizeentries,
        ]:
            for entry in entries:
                self.assertEqual(entry.get(), "")

        self.assertEqual(gui.flag_crosshair.get(), True)
        self.assertEqual(gui.flag_voi.get(), False)
        self.assertEqual(gui.flag_zoom.get(), False)

        self.assertIsNone(gui.traframe.image)
        self.assertIsNone(gui.corframe.image)
        self.assertIsNone(gui.sagframe.image)
        self.assertEqual(gui.traframe.arrays, [])
        self.assertEqual(gui.corframe.arrays, [])
        self.assertEqual(gui.sagframe.arrays, [])

    def test_init_1img(self):
        gui = self.GUI(self.aim2)
        gui.samplerate.set(1 / gui.imgs[0].affine.scale()[0])
        gui.flag_render.set(True)

        for dropdown, expected_val in zip(
            gui.imgselect.dropdowns, [self.aim2.filename, "None"]
        ):
            self.assertEqual(dropdown._variable.get(), expected_val)
            self.assertEqual(dropdown.get_options(), ["None", self.aim2.filename])

        for entry, scale, expected_val, max_val in zip(
            gui.imgpos.entries,
            gui.imgpos.scales,
            [(dimshape - 1) // 2 for dimshape in self.aim2.shape],
            self.aim2.shape,
        ):
            self.assertEqual(float(entry.get()), expected_val)
            self.assertEqual(scale.get(), expected_val)
            self.assertEqual(scale.cget("from"), max_val - 1)
            self.assertEqual(scale.cget("to"), 0)

        for entries, expected_vals in zip(
            [
                gui.voiinfo.posentries,
                gui.voiinfo.shapeentries,
                gui.voiinfo.elsizeentries,
            ],
            [[0, 0, 0], self.aim2.shape, self.aim2.affine.scale()],
        ):
            for entry, expected_val in zip(entries, expected_vals):
                self.assertEqual(
                    float(entry.get()),
                    expected_val,
                    f"entry:{entry.get()}; expected:{expected_val}",
                )

        self.np.assert_equal(
            gui.traframe.arrays[0],
            gui.imgs[0][16],
        )
        self.np.assert_equal(
            gui.corframe.arrays[0],
            np.flipud(gui.imgs[0][:, 26]),
        )
        self.np.assert_equal(
            gui.sagframe.arrays[0],
            gui.imgs[0][:, :, 24].T,
        )

    def test_init_2imgs_sync_w_gui(self):
        gui = self.GUI(self.npy2, self.npy3)

        self.assert_dropdowns(
            gui.imgselect.dropdowns,
            [self.npy2.filename, self.npy3.filename],
            ["None", self.npy2.filename, self.npy3.filename],
        )

        for entry, scale, expected_val, max_val in zip(
            gui.imgpos.entries,
            gui.imgpos.scales,
            [(dimshape - 1) // 2 for dimshape in self.npy2.shape],
            [self.npy3.shape[0], *self.npy2.shape[1:]],
        ):
            self.assertEqual(float(entry.get()), expected_val)
            self.assertEqual(scale.get(), expected_val)
            self.assertEqual(scale.cget("from"), max_val - 1)
            self.assertEqual(scale.cget("to"), 0)

        for entries, expected_vals in zip(
            [
                gui.voiinfo.posentries,
                gui.voiinfo.shapeentries,
                gui.voiinfo.elsizeentries,
            ],
            [[0, 0, 0], self.npy2.shape, self.npy2.affine.scale()],
        ):
            for entry, expected_val in zip(entries, expected_vals):
                self.assertEqual(
                    float(entry.get()),
                    expected_val,
                    f"entry:{entry.get()}; expected:{expected_val}",
                )

    def assert_dropdowns(self, dropdowns, expected_vals, options=()):
        for dropdown, expected_val in zip(dropdowns, expected_vals):
            self.assertEqual(dropdown._variable.get(), expected_val)
            if options:
                self.assertEqual(dropdown.get_options(), list(options))

    def test_dropdowns_update(self):
        gui = self.GUI(self.npy2, self.npy3)
        callbacks = gui.imgselect.generate_callbacks(gui.get_options())

        callbacks[0]["None"]()
        self.assert_dropdowns(gui.imgselect.dropdowns, [self.npy3.filename, "None"])
        callbacks[0]["None"]()
        self.assert_dropdowns(gui.imgselect.dropdowns, ["None", "None"])
        callbacks[1][self.npy3.filename]()
        self.assert_dropdowns(gui.imgselect.dropdowns, [self.npy3.filename, "None"])

    def test_empty_entry(self):
        gui = self.GUI(self.npy2)

        entry = gui.imgpos.entries[0]
        entry.invalidfunction("0", "", "0", 1, "key", 0)
        self.assertEqual(entry.get(), "")

    def test_push_empty(self):
        aim = img.AIM(self.aim3name, verbosity=1).load()
        gui = self.GUI(aim)
        gui.imgpos.entries[0].invalidfunction("0", "", "0", "0", "key", "0")
        gui.voiinfo.posentries[0].invalidfunction("0", "", "0", "0", "key", "0")
        gui.voiinfo.shapeentries[0].invalidfunction("0", "", "0", "0", "key", "0")
        gui.voiinfo.elsizeentries[0].invalidfunction("0", "", "0", "0", "key", "0")
        gui.push_pos(0)
        gui.push_voielsize(0)
        gui.push_voipos(0)
        gui.push_voishape(0)
        self.assertEqual(gui.imgpos.entries[0].get(), "")
        self.np.assert_equal(aim.position, [[132], [78], [90]])

    def test_bug_samplerate_missizing_w_smaller_primaryimg(self):
        gui = self.GUI(self.npy3name, self.aim3name)
        gui.flag_render.set(True)
        opposite_gui = self.GUI(self.aim3name, self.npy3name)
        opposite_gui.flag_render.set(True)
        self.assertEqual(gui.samplerate.get(), opposite_gui.samplerate.get())
        self.assertEqual(
            gui.traframe.image.height(), opposite_gui.traframe.image.height()
        )
