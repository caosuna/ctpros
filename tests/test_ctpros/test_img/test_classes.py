import unittest

from ctpros.img.classes import *


class ClassTester(unittest.TestCase):
    """Template TestCase suite which provides shortcuts for NumPy testing and directories."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.verbosity = 1

        infolders = ["data", "input"]
        interimfolders = ["data", "interim"]
        suppfolders = ["data", "supplement"]
        outfolders = ["data", "output"]
        self.dirin = os.path.abspath(os.path.join(*infolders)) + os.path.sep
        self.dirinterim = os.path.abspath(os.path.join(*interimfolders)) + os.path.sep
        self.dirout = os.path.abspath(os.path.join(*outfolders)) + os.path.sep
        self.dirsupp = os.path.abspath(os.path.join(*suppfolders)) + os.path.sep

        self.notaffine = "test_notanimg.txt"
        self.affinename = "test_affine.tfm"
        self.voiname = "test_voi.voi"
        self.npyv2 = "test_v2.npy"
        self.aimv2 = "test_v2.aim;1"
        self.aimv2_gobj = "test_v2_gobj.aim;1"
        self.npyv3 = "test_v3.npy"
        self.aimv3 = "test_v3.aim;1"
        self.voidne = "test_voidne.voi"
        self.mda = "test.mda"

    def setUp(self):
        pass

    def tearDown(self):
        """
        Empty interim and output test datafolders

        """
        for folder in [self.dirinterim, self.dirout]:
            for root, _, files in os.walk(folder):
                for f in files:
                    if f.lower() != "readme.md":
                        os.unlink(os.path.join(root, f))


class TestAffine(ClassTester):
    def test_snap(self):
        affine = AffineTensor(2).rotate(0.01)
        affine.rotate(-0.01)
        self.np.assert_equal(affine, np.eye(3))

    def test_translate(self):
        affine = AffineTensor(3)

        translates = np.array([10, 20, 30])
        affine.translate(*translates)
        self.np.assert_equal(affine.translate(), translates)

    def test_rotate1d(self):
        affine = AffineTensor(1)
        with self.assertRaises(Exception):
            affine.rotate()

    def test_rotate2d(self):
        affine = AffineTensor(2)

        theta = 0.01
        affine.rotate(theta)
        expected = np.array(
            [
                [np.cos(theta), -np.sin(theta), 0],
                [np.sin(theta), np.cos(theta), 0],
                [0, 0, 1],
            ]
        )
        self.np.assert_equal(expected, affine)
        self.np.assert_almost_equal(affine.rotate(), (theta,))

    def test_rotate3d(self):
        angles = np.array([0.01, 0.02, 0.03])
        affine = AffineTensor(3).rotate(*angles)
        self.np.assert_almost_equal(affine.rotate(), angles, 4)

    def test_rotate4d(self):
        angles = np.array([0.01, 0.02, 0.03, 0.04, 0.05, 0.06])
        affine = AffineTensor(4).rotate(*angles)
        self.np.assert_almost_equal(affine.rotate(), angles, 4)

    def test_rotate_dimensionality(self):
        with self.assertRaises(Exception):
            AffineTensor(3).rotate(1, 2)

    def test_scale(self):
        affine = AffineTensor(3)

        scales = np.array([2, 2, 2])
        affine.scale(*scales)
        self.np.assert_equal(affine.scale(), scales)

        otheraffine = AffineTensor(3).scale(2)
        self.np.assert_equal(affine, otheraffine)

    def test_affine(self):
        affine = AffineTensor(3)

        updater = np.ndarray((4, 4))
        affine.affine(updater)
        self.np.assert_equal(affine, updater)

    def test_translate_dimensionality(self):
        with self.assertRaises(Exception):
            AffineTensor(3).translate(1)

    def test_load(self):
        fromfile = AffineTensor(self.dirin + self.affinename)
        self.np.assert_equal(
            fromfile,
            AffineTensor(3)
            .rotate(0.01, 0.02, 0.03)
            .scale(2, 3, 4)
            .translate(10, 20, 30),
        )

    def test_saveas(self):
        affine = (
            AffineTensor(3)
            .rotate(0.01, 0.02, 0.03)
            .scale(2, 3, 4)
            .translate(10, 20, 30)
        )
        affine.saveas(self.dirout + self.affinename)
        loaded = AffineTensor(self.dirout + self.affinename)
        self.np.assert_equal(affine, loaded)

    def test_filenameisproper(self):
        with self.assertRaises(Exception):
            AffineTensor("notafile.mp3")
        with self.assertRaises(Exception):
            AffineTensor(self.dirin + self.notaffine)

    def test_decomposition_wscale(self):
        rotates = (0, np.pi / 3, np.pi / 4)
        scales = (3, 4, 5)
        translates = (1, 2, 3)
        affine = AffineTensor(3).rotate(*rotates).scale(*scales).translate(*translates)
        T, S, R, A = affine.decomposition("TSRA")
        product = np.dot(np.dot(np.dot(T, S), R), A)
        self.np.assert_almost_equal(product, affine)
        self.np.assert_equal(T.scale(), 1)
        self.np.assert_equal(T.rotate(), 0)
        self.np.assert_equal(T.translate(), affine.translate())
        self.np.assert_equal(S.scale(), affine.scale())
        self.np.assert_equal(S.rotate(), 0)
        self.np.assert_equal(S.translate(), 0)
        self.np.assert_equal(R.scale(), 1)
        self.np.assert_almost_equal(R.rotate(), affine.rotate())
        self.np.assert_equal(R.translate(), 0)
        self.np.assert_almost_equal(A, np.eye(affine.pdim + 1))

        with self.assertRaises(Exception):
            affine.decomposition("X")

    def test_decomposition_wstretch(self):
        stretches = (1, 0.2, 0.3, 4, 0.5, 6)
        rotates = (0, np.pi / 3, np.pi / 4)
        affine = AffineTensor(3).stretch(*stretches).rotate(*rotates)
        R, V = affine.decomposition("RV")
        self.np.assert_almost_equal(R.rotate(), (0, np.pi / 3, np.pi / 4))
        self.np.assert_almost_equal(V.stretch(), (1, 0.2, 0.3, 4, 0.5, 6))

    def test_voi(self):
        voi = VOI(pos=[1, 2, 3], shape=[2, 2, 2], elsize=[2.0, 2.0, 2.0])
        affine, shape = AffineTensor(3).voi(voi)
        self.np.assert_equal(affine.translate(), [-0.5, -1, -1.5])
        self.np.assert_equal(affine.scale(), [0.5, 0.5, 0.5])
        self.np.assert_equal(shape, [2, 2, 2])

    def test_flip(self):
        physshape = [20, 30]
        affine = AffineTensor(2).scale(2).flip(0, 1, physshape=physshape)
        self.assertEqual(affine[0, 0], -2)
        self.assertEqual(affine[1, 1], -2)
        self.assertEqual(affine[0, -1], 18)
        self.assertEqual(affine[1, -1], 28)

    def test_stretch_indefinite(self):
        stretches = [1, 2, 3]
        with self.assertRaises(Exception):
            AffineTensor(2).stretch(*stretches)

    def test_stretch_impropernumber(self):
        stretches = [1, 2, 3]
        with self.assertRaises(Exception):
            AffineTensor(3).stretch(*stretches)

    def test_stretch(self):
        stretches = [1, 0.5, 3]
        affine = AffineTensor(2).stretch(*stretches)
        self.np.assert_equal(affine.dot([1, 0]), [1, 0.5])
        self.np.assert_equal(affine.dot([0, 1]), [0.5, 3])

    def test_stretch_get(self):
        stretches = [1, 0.5, 3]
        affine = AffineTensor(2).stretch(*stretches)
        newaffine = AffineTensor(2).stretch(*affine.stretch())
        self.np.assert_almost_equal(newaffine.dot([1, 0]), [1, 0.5])
        self.np.assert_equal(newaffine.dot([0, 1]), [0.5, 3])

    def test_align(self):
        affine = AffineTensor(2).scale(2).rotate(0.1)
        aligner = AffineTensor(2).rotate(0.2).translate(1, 2)
        affine.align(aligner)
        self.np.assert_almost_equal(affine.rotate(), (0.2,))
        self.np.assert_almost_equal(affine.translate(), (1, 2))


class TestVOI(ClassTester):
    def test_init_empty(self):
        voi = VOI()
        self.np.assert_equal(voi.pos, [[0], [0], [0]])
        self.assertEqual(voi.pos.dtype, np.float64)
        self.np.assert_equal(voi.shape, [0, 0, 0])
        self.assertEqual(voi.shape.dtype, np.uint64)
        self.np.assert_equal(voi.elsize, [1, 1, 1])
        self.assertEqual(voi.elsize.dtype, np.float64)

        self.assertIsNone(voi.filename)
        self.assertEqual(voi.fileloc, "." + os.path.sep)

    def test_init_filename_proper(self):
        voi = VOI(self.dirin + self.voiname)
        self.assertEqual(voi.filename, self.voiname)
        self.assertEqual(voi.fileloc, self.dirin)
        self.np.assert_equal(voi.pos, [[1], [1], [1]])
        self.np.assert_equal(voi.shape, [1, 1, 1])
        self.np.assert_equal(voi.elsize, [10, 10, 10])

    def test_init_filename_notvoi_dne_nonstr(self):
        with self.assertRaises(Exception):
            VOI(self.dirin + self.npyv2)
        with self.assertRaises(Exception):
            VOI(self.dirin + self.voidne)
        with self.assertRaises(Exception):
            VOI(["this is in a list"])

    def test_save(self):
        voi = VOI(self.dirin + self.voiname)
        voi.saveas(self.dirout + self.voiname)
        self.assertEqual(voi.fileloc, self.dirout)

        voi = VOI(self.dirout + self.voiname)
        oldvoi = VOI(self.dirin + self.voiname)
        self.np.assert_equal(voi.pos, oldvoi.pos)
        self.np.assert_equal(voi.shape, oldvoi.shape)
        self.np.assert_equal(voi.elsize, oldvoi.elsize)

        with self.assertRaises(Exception):
            voi.saveas("not/a/dir/test_voi.voi")

    def test_copy(self):
        voi = VOI()
        copied = voi.copy()
        copied.pos[0] += 1
        self.assertNotEqual(copied.pos[0], voi.pos[0])


class TestNDArray(ClassTester):
    def __init__(self, *args, imgclass=NDArray, **kwargs):
        super().__init__(*args, **kwargs)
        self.__init_params__(imgclass)

        self.filename_badname = self.dirin + "dne" + os.path.splitext(self.filename)[-1]
        self.filename_badext = self.dirin + self.affinename
        self.filename_baddir = "dne" + os.path.sep + os.path.splitext(self.filename)[-1]
        self.generate_baseimg = lambda: self.ImgClass(
            *self.base_args, **self.base_kwargs
        )
        self.child_array = np.ndarray(*self.base_args, **self.base_kwargs)
        self.child_ndimg1 = NDArray(self.dirin + self.npyv2, verbosity=self.verbosity)
        self.child_ndimg2 = NDArray(self.dirin + self.npyv3, verbosity=self.verbosity)

    def __init_params__(self, imgclass):
        self.ImgClass = lambda *args, **kwargs: imgclass(
            *args, verbosity=self.verbosity, **kwargs
        )
        self.filename = self.dirin + self.npyv3
        self.savename = self.dirout + self.npyv3
        self.numpy_params = {"args": ((2, 3),), "kwargs": {"dtype": np.uint8}}
        self.numpy_params_bad = [
            {"args": (), "kwargs": {}},
            {"args": ((2, 3),), "kwargs": {"shape": ((2, 3))}},
        ]

        self.base_args = ((2, 3),)
        self.base_kwargs = {"dtype": np.uint8}
        self.alt_args = ((3, 2),)
        self.alt_kwargs = {"dtype": np.int8}

    def setUp(self):
        self.img = self.generate_baseimg()
        self.img.ravel()[:] = np.arange(self.img.size)

    def tearDown(self):
        super().tearDown()
        self.child_ndimg1.clear()
        self.child_ndimg2.clear()

    def test_init_params_NumPy(self):
        args = self.numpy_params["args"]
        kwargs = self.numpy_params["kwargs"]
        ndarray = np.ndarray(*args, **kwargs)
        ndimg = NDArray(*args, **kwargs)
        self.assertEqual(ndarray.dtype, ndimg.dtype)
        self.assertEqual(ndarray.shape, ndimg.shape)

    def test_init_arrays_get_casted(self):
        ndimg = self.ImgClass(self.child_array)
        self.np.assert_equal(ndimg, self.child_array)
        self.assertEqual(ndimg.dtype, self.child_array.dtype)

    def test_init_ndimgs_get_casted(self):
        ndimg = self.ImgClass(self.child_ndimg1)
        ndimg == self.child_ndimg1
        self.np.assert_equal(ndimg, self.child_ndimg1)
        self.assertEqual(ndimg.dtype, self.child_ndimg1.dtype)
        self.assertEqual(ndimg.filename, self.child_ndimg1.filename)

    def test_init_array_and_img_casted(self):
        ndimg = NDArray(self.child_array, self.child_ndimg1)
        self.np.assert_equal(ndimg, self.child_array)
        self.assertEqual(ndimg.filename, self.child_ndimg1.filename)

    def test_init_img_and_img_casted(self):
        ndimg = NDArray(self.child_ndimg1, self.child_ndimg2)
        self.np.assert_equal(ndimg, self.child_ndimg1)
        self.assertEqual(ndimg.filename, self.child_ndimg2.filename)

    def test_validforIO(self):
        self.assertTrue(self.img.validforIO())
        self.img.dtype = "|S1"
        self.assertFalse(self.img.validforIO())
        with self.assertRaises(Exception):
            self.img.validforIO(raiseit=True)

    def test_str(self):
        self.assertEqual(
            self.img.__str__(),
            f"{type(self.img).__name__}(shape = {self.img.shape}, dtype = {self.img.dtype}, filename = '{self.img.filename}')",
        )

    def test_reset_affine(self):
        self.img.affine.scale(2)
        self.img.reset_affine()
        self.np.assert_equal(self.img.affine.scale(), 1)

    def test_init_filename(self):
        ndimg = self.ImgClass(self.filename)
        self.assertTrue(ndimg.validforIO())

    def test_init_filename_bad(self):
        for badfilename in [
            self.filename_baddir,
            self.filename_badext,
            self.filename_badname,
        ]:
            with self.assertRaises(Exception):
                self.ImgClass(badfilename)

    def test_IO_loop(self):
        ndimg = self.ImgClass(self.filename).load()
        ndimg.saveas(self.savename)
        newimg = self.ImgClass(self.savename).load()

        self.np.assert_equal(ndimg, newimg)
        self.np.assert_equal(ndimg.header, newimg.header)

    def test_IO_indexed(self):
        ndimg = self.ImgClass(self.filename).load()
        ndimg[1:4, 1:4, 1:4].saveas(self.savename)
        newimg = self.ImgClass(self.savename).load()
        self.np.assert_equal(newimg, ndimg[1:4, 1:4, 1:4])

    def test_IO_nofilename(self):
        with self.assertRaises(Exception):
            NDArray((1, 2, 3)).load()

    def test_saveas_badext(self):
        with self.assertRaises(Exception):
            self.ImgClass(self.filename).load().saveas(
                self.dirout + self.filename_badext
            )

    def test_squeeze(self):
        ndimg = NDArray((1, 2, 3))
        self.assertEqual(ndimg.squeeze(inplace=False).shape, (2, 3))
        ndimg.squeeze()
        self.assertEqual(ndimg.shape, (2, 3))

    def test_fill(self):
        self.img.fill(0)
        self.np.assert_equal(self.img, np.zeros_like(self.img))

        ndimg = self.ImgClass(*self.alt_args, **self.alt_kwargs)
        ndimg[:] = 1
        self.img.fill(ndimg)
        self.np.assert_equal(ndimg, self.img)

    def test_reload_behavior(self):
        with self.assertRaises(Exception):
            self.img.load()
        self.img.clear()
        with self.assertRaises(Exception):
            self.img.load()

        img = self.ImgClass(self.filename).load()
        original = img.ravel()[0]
        img += 1
        self.assertEqual(img.reload().ravel()[0], original)

    def test_ufunc_out_None(self):
        newimg = np.sin(self.img)
        self.assertFalse((newimg == self.img).all())
        self.assertTrue(newimg.flags.owndata)
        self.assertTrue(issubclass(type(newimg), ImgTemplate))

    def test_ufunc_out_1(self):
        self.img = np.real(self.img).astype(np.single)
        newimg = np.sin(self.img, out=self.img)
        self.np.assert_equal(newimg, self.img)
        self.assertTrue(newimg.flags.owndata)
        self.assertIs(newimg, self.img)

    def test_ufunc_dtype(self):
        newimg = np.conj(self.img, dtype=np.complex64)
        self.assertFalse(newimg is self.img)
        self.assertTrue(newimg.flags.owndata)
        self.assertTrue(issubclass(type(newimg), ImgTemplate))
        self.assertEqual(newimg.dtype, np.complex64)

    def test_view_defaults_to_ndarray(self):
        self.assertEqual(type(self.img.view()), np.ndarray)


class TestAIM(TestNDArray):
    def __init__(self, *args, imgclass=AIM, **kwargs):
        super().__init__(*args, imgclass=imgclass, **kwargs)
        self.child_ndimg1 = ImgTemplate(
            self.dirin + self.aimv2, verbosity=self.verbosity
        )
        self.child_ndimg2 = ImgTemplate(
            self.dirin + self.aimv3, verbosity=self.verbosity
        )

    def __init_params__(self, imgclass):
        super().__init_params__(imgclass)
        self.filename = self.dirin + self.aimv3
        self.savename = self.dirout + self.aimv3
        self.numpy_params = {"args": ((2, 3, 2),), "kwargs": {"dtype": np.int16}}
        self.numpy_params_bad = [
            {"args": (), "kwargs": {}},
            {"args": ((2, 3, 2),), "kwargs": {"dtype": np.uint8}},
            {"args": ((2, 3),), "kwargs": {"dtype": np.int16}},
            {"args": ((2, 3, 2),), "kwargs": {"shape": ((2, 3, 2))}},
        ]

        self.base_args = ((2, 3, 2),)
        self.base_kwargs = {"dtype": np.int16}
        self.alt_args = ((3, 2, 3),)
        self.alt_kwargs = {"dtype": np.int8}
        self.v2_header, self.v3_header, self.v2_gobj_header = self._mkAIMheaders()

    def _mkAIMheaders(self):
        v2_logname = "test_v2_processlog.npy"
        fulllogname = os.path.join(self.dirin, v2_logname)
        processlog = np.load(fulllogname)
        v2 = {
            "version": np.array([], dtype=np.uint32),
            "blocksize": np.array([20, 140, 3037, 183600, 0], dtype=np.uint32),
            "hidden #1": np.array([16, 0, 0, 0, 0, 131074], dtype=np.uint32),
            "dim": np.array([50, 54, 34], dtype=np.uint32),
            "pos": np.array([0, 0, 0], dtype=np.uint32),
            "off": np.array([0, 0, 0], dtype=np.uint32),
            "subdim": np.array([0, 0, 0], dtype=np.uint32),
            "supdim": np.array([0, 0, 0], dtype=np.uint32),
            "suppos": np.array([0, 0, 0], dtype=np.uint32),
            "testoff": np.array([0, 0, 0], dtype=np.uint32),
            "elsize": np.array(
                [196, 60, 166, 155, 196, 60, 166, 155, 196, 60, 166, 155],
                dtype=np.uint8,
            ),
            "hidden #2": np.array([0, 0, 0, 0, 0], dtype=np.uint32),
            "process log": processlog,
            "hidden #3": np.array([], dtype=np.uint32),
        }
        v3_logname = "test_v3_processlog.npy"
        fulllogname = os.path.join(self.dirin, v3_logname)
        processlog = np.load(fulllogname)
        v3 = {
            "version": np.array(b"AIMDATA_V030   \x00", dtype="|S16"),
            "blocksize": np.array([40, 224, 3037, 79856, 0], dtype=np.uint64),
            "hidden #1": np.array([24, 0, 0, 131074], dtype=np.uint32),
            "dim": np.array([31, 28, 46], dtype=np.uint64),
            "pos": np.array([247, 419, 0], dtype=np.uint64),
            "off": np.array([0, 0, 0], dtype=np.uint64),
            "supdim": np.array([0, 0, 0], dtype=np.uint64),
            "suppos": np.array([0, 0, 0], dtype=np.uint64),
            "subdim": np.array([0, 0, 0], dtype=np.uint64),
            "testoff": np.array([0, 0, 0], dtype=np.uint64),
            "elsize": np.array([60000, 60000, 60000], dtype=np.uint64),
            "hidden #2": np.array([0, 0, 0, 0], dtype=np.uint32),
            "process log": processlog,
            "hidden #3": np.array([], dtype=np.uint32),
        }

        v2_gobj_logname = "test_v2_gobj_processlog.npy"
        processlog = np.load(self.dirin + v2_gobj_logname)
        v2_gobj = {
            "version": np.array([], dtype=np.uint32),
            "blocksize": np.array([20, 140, 6014, 12167, 0], dtype=np.uint32),
            "hidden #1": np.array([16, 0, 0, 0, 0, 65537], dtype=np.uint32),
            "dim": np.array([23, 23, 23], dtype=np.uint32),
            "pos": np.array([0, 0, 0], dtype=np.uint32),
            "off": np.array([0, 0, 0], dtype=np.uint32),
            "supdim": np.array([0, 0, 0], dtype=np.uint32),
            "suppos": np.array([0, 0, 0], dtype=np.uint32),
            "subdim": np.array([0, 0, 0], dtype=np.uint32),
            "testoff": np.array([0, 0, 0], dtype=np.uint32),
            "elsize": np.array(
                [170, 61, 90, 100, 170, 61, 90, 100, 170, 61, 195, 100], dtype=np.uint8
            ),
            "hidden #2": np.array([0, 0, 0, 0, 0], dtype=np.uint32),
            "process log": processlog,
            "hidden #3": np.array([], dtype=np.uint32),
        }
        return v2, v3, v2_gobj

    def test_IO_header(self):
        self.np.assert_equal(
            self.ImgClass(self.dirin + self.aimv2).header, self.v2_header
        )
        self.np.assert_equal(
            self.ImgClass(self.dirin + self.aimv2_gobj).header, self.v2_gobj_header
        )
        self.np.assert_equal(
            self.ImgClass(self.dirin + self.aimv3).header, self.v3_header
        )

    def test_IO_loop_affine2elsize(self):
        aim = self.ImgClass(self.dirin + self.aimv2)
        self.np.assert_equal(aim._affine2elsize(), aim.header["elsize"])

    def test_IO_loop_v2(self):
        self.img._versionchange(2)
        self.assertEqual(self.img._version(), 2)
        self.img.saveas(self.savename)
        newimg = self.ImgClass(self.savename).load()
        self.np.assert_equal(self.img, newimg)
        self.np.assert_equal(self.img.header, newimg.header)

    def test_IO_indexed(self):
        ndimg = self.ImgClass(self.dirin + self.aimv2).load()
        indexed = ndimg[1:4, 1:4, 1:4]
        indexed.saveas(self.savename)
        newimg = self.ImgClass(self.savename).load()
        self.np.assert_equal(newimg, ndimg[1:4, 1:4, 1:4])
        self.np.assert_equal(newimg.affine, ndimg.affine)

    def test_convert_version_loop(self):
        aimv3 = self.ImgClass((0, 0, 0))
        aimv2 = aimv3._versionchange(2, inplace=False)

        defaultheader2 = {
            "version": np.array([], dtype=np.uint32),
            "blocksize": np.array([20, 140, 1, 0, 0], dtype=np.uint32),
            "hidden #1": np.array([16, 0, 0, 0, 0, 131074], dtype=np.uint32),
            "dim": np.array((0, 0, 0), dtype=np.uint32),
            "pos": np.array([0, 0, 0], dtype=np.uint32),
            "off": np.array([0, 0, 0], dtype=np.uint32),
            "supdim": np.array([0, 0, 0], dtype=np.uint32),
            "suppos": np.array([0, 0, 0], dtype=np.uint32),
            "subdim": np.array([0, 0, 0], dtype=np.uint32),
            "testoff": np.array([0, 0, 0], dtype=np.uint32),
            "elsize": np.array(
                [131, 59, 111, 18, 131, 59, 111, 18, 131, 59, 111, 18], dtype=np.uint8
            ),
            "hidden #2": np.array([0, 0, 0, 0, 0], dtype=np.uint32),
            "process log": np.array(b"", dtype="|S1"),
            "hidden #3": np.array([], dtype=np.uint32),
        }

        for key, value in defaultheader2.items():
            self.np.assert_equal(value, aimv2.header[key])

        newaimv3 = aimv2._versionchange(3, inplace=False)
        self.np.assert_equal(aimv3, newaimv3)
        for key, value in aimv3.header.items():
            self.np.assert_equal(value, newaimv3.header[key])

    def test_versionchange_same(self):
        self.assertIs(
            self.img, self.img._versionchange(self.img._version(), inplace=False)
        )


class TestJPEG_Gray(ClassTester):
    def test_init(self):
        im = JPEG("data/input/test_grayjpg.jpg").load()
        self.assertEqual(im.shape, (8500, 8500))
        self.assertEqual(im.dtype, np.uint8)


class TestMDA(TestNDArray):
    def __init__(self, *args, imgclass=MDA, **kwargs):
        super().__init__(*args, imgclass=imgclass, **kwargs)
        self.child_ndimg1 = ImgTemplate(self.dirin + self.mda, verbosity=self.verbosity)
        self.child_ndimg2 = ImgTemplate(self.dirin + self.mda, verbosity=self.verbosity)

    def __init_params__(self, imgclass):
        super().__init_params__(imgclass)
        self.filename = self.dirin + self.mda
        self.savename = self.dirout + self.mda
        self.numpy_params = {"args": ((2, 3, 2),), "kwargs": {"dtype": np.complex64}}
        self.numpy_params_bad = [
            {"args": (), "kwargs": {}},
            {"args": ((2, 3, 2),), "kwargs": {"dtype": np.uint8}},
            {"args": ((2, 3),), "kwargs": {"dtype": np.int16}},
            {"args": ((2, 3, 2),), "kwargs": {"shape": ((2, 3, 2))}},
        ]

        self.base_args = ((2, 3, 2),)
        self.base_kwargs = {"dtype": np.complex64}
        self.alt_args = ((3, 2),)
        self.alt_kwargs = {"dtype": np.float32}

    # def test_init(self):
    #     im = MDA(f"{self.dirin}{self.mda}")
    #     self.np.assert_equal([41, 41, 41], im.header["shape"])
    #     self.np.assert_equal("complex64", im.header["dtype"])
    #     self.np.assert_equal(16, im.header["headersize"])
    #     self.np.assert_equal(68921, im.header["datasize"])

    #     self.np.assert_equal(im.shape, (0, 0, 0))
    #     self.assertEqual(im.dtype, "complex64")

    # def test_load(self):
    #     im = MDA(f"{self.dirin}{self.mda}").load()

    # self.assertEqual(im.shape, (41, 41, 41))
    # self.assertEqual(im.dtype, "complex64")
