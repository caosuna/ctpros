import unittest
import numpy as np

import ctpros
from ctpros.img.utils.script import *


class TestSet(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_updatedict(self):
        mydict = {"Perma1": "Val1", "Perma2": "Val2"}
        updater = {"Perma2": "NewVal", "Temp1": "TempVal1"}
        expected = {"Perma1": "Val1", "Perma2": "NewVal"}
        self.assertEqual(updatedict(mydict, updater), expected)
        with self.assertRaises(Exception):
            updatedict(mydict, updater, mode="not selected")

    def test_maxshape_original(self):
        img = ctpros.NDArray((3, 3, 3))
        img.affine.scale(1, 2, 3)
        otherimg = img.copy()
        otherimg.affine.scale(3, 1, 1)

        img.affine.rotate(0.1, 0.2, 0.3)
        otherimg.affine.rotate(1, 2, 3)
        self.np.assert_almost_equal(maxshape(img, otherimg), [9, 6, 9])

    def test_maxshape_current(self):
        img = ctpros.NDArray((3, 3, 3))
        img.affine.scale(1, 2, 3)
        img.affine.rotate(0.1, 0.2, 0.3)

        otherimg = img.copy()
        otherimg.affine.scale(3, 1, 1)
        otherimg.affine.rotate(1, 2, 3)
        self.assertFalse((maxshape(img, otherimg, frame="current") == [9, 6, 9]).all())

    def test_maxshape_exc(self):
        img = ctpros.NDArray((3, 3))
        with self.assertRaises(Exception):
            maxshape(img, frame="undefined")
